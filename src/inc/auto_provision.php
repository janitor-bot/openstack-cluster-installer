<?php

function auto_provision_add_machine($con, $conf, $machine_id){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    # Check if the machine exists
    $q = "SELECT * FROM machines WHERE id='$machine_id'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n < 1){
        $json["status"] = "error";
        $json["message"] = "Cannot find a machine with id $machine_id.";
        return $json;
    }
    $machine = mysqli_fetch_array($r);

    $machine_status  = $machine["status"];
    $machine_id      = $machine["id"];
    $machine_cluster = $machine["cluster"];

    // Check status of that machine
    if($machine_status != "live"){
        $json["status"] = "error";
        $json["message"] = "Error: machine isn't running live image.";
        return $json;
    }
    if(!is_null($machine["cluster"])){
        $json["status"] = "error";
        $json["message"] = "Error: machine already enrolled in a cluster.";
        return $json;
    }

    if($machine["cluster"] !== NULL){
        $json["status"] = "error";
        $json["message"] = "Error: machine with serial $safe_machine_serial is already enrolled.";
        return $json;
    }

    $safe_cluster_name = $conf["auto_provision"]["auto_add_machines_cluster_name"];
    $r = mysqli_query($con, "SELECT * FROM clusters WHERE name='$safe_cluster_name'");
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n != 1){
        $json["status"] = "error";
        $json["message"] = "Error: cluster name $safe_cluster_name doesn't exist.";
        return $json;
    }
    $cluster = mysqli_fetch_array($r);
    $safe_cluster_id = $cluster["id"];

    $ret = hardware_profile_guess_profile($con, $conf, $machine["id"]);

    if(sizeof($ret["data"]) != 1){
        $json["status"] = "error";
        $json["message"] = "Cannot guess hardware profile: reply from hardware_profile_guess_profile() does not show 1 reply.";
        return $json;
    }
    $hardware_profile_name = $ret["data"][0];

    $hardware_profile = hardware_profile_load_profile($con, $conf, $hardware_profile_name);
    $safe_role_name = $hardware_profile["data"]["role"];

    $ret = auto_racking_guess_location($con, $conf, $machine["id"]);
    if($ret["status"] != "success"){
        return $ret;
    }
    if(!isset($ret["data"]) || sizeof($ret["data"]) == 0){
        $json["status"] = "error";
        $json["message"] = "Cannot guess racking location.";
        return $json;
    }
    $switch_hostname = $ret["data"]["switch_hostname"];


    $ret = load_racking_info_config($con, $conf);
    if($ret["status"] != "success"){
        return $ret;
    }
    $racking_info = $ret["data"];

    if( !isset($racking_info["switchhostnames"][ $switch_hostname ]["location-name"]) ){
        $json["status"] = "error";
        $json["message"] = "Cannot find location matching switch hostname: $switch_hostname";
        return $json;
    }
    $location_name = $racking_info["switchhostnames"][ $switch_hostname ]["location-name"];

#    echo "Here, location_name: $location_name\n";

    $q = "SELECT id FROM locations WHERE name='".$location_name."'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n != 1){
        $json["status"] = "error";
        $json["message"] = "Error: location name $location_name doesn't exist.";
        return $json;
    }
    $location = mysqli_fetch_array($r);
    $safe_location_id = $location["id"];

//    echo "Adding machine ".$machine["serial"]." to cluster ".$cluster["name"]." with role ".$safe_role_name." and location ".$safe_location_id.".\n";
    $json = add_node_to_cluster($con, $conf, $machine["id"], $safe_cluster_id, $safe_role_name, $safe_location_id);
    return $json;
}

?>
